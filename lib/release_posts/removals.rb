require 'nokogiri'
require 'yaml'

module ReleasePosts
  class Removals
    include Helpers

    def initialize
      @removals_url = "https://docs.gitlab.com/ee/update/removals.html"
      @removals_dir = File.join(git_repo_path, 'data', 'release_posts')
      @removals_file = File.join(@removals_dir, 'removals-index.yml')
      @removals = {}
    end

    def generate
      find_removals
      save_removals
      commit_removals
    end

    private

    def page
      Nokogiri::HTML(URI.open(@removals_url))
    end

    def find_removals
      milestones ||= page.css('.article-content h2')

      # Iterate over each h2
      milestones.each do |milestone|
        # Get the release number
        release = milestone.content.tr('Removed in ', '').tr("\n", '')

        # Initialize an array for the release if it doesn't exist
        @removals[release] ||= []

        # Get all subsequent siblings until the next h2
        h3s = []
        sibling = milestone.next_sibling
        until sibling.nil? || sibling.name == "h2"
          h3s << sibling.content.tr("\n", '') if sibling.name == "h3"

          sibling = sibling.next_sibling
        end

        # Add the h3s to the array
        @removals[release].concat(h3s)
      end
    end

    def save_removals
      # Write the removals to the YAML file
      File.write(@removals_file, @removals.to_yaml)
    end

    def commit_removals
      puts "Fetching branches"
      git_fetch

      puts 'Set username and email'
      git_config('user.email', 'job+bot@gitlab.com')
      git_config('user.name', 'Bot')

      puts "Switching branch to #{next_release}"
      git_change_branch(next_release)

      git_add(@removals_file)
      git_commit("Updated removals content")
      git_push("https://jobbot:#{ENV.fetch('GITLAB_BOT_TOKEN', nil)}@gitlab.com/gitlab-com/www-gitlab-com.git", next_release)
    end

    def fetch_release_branches
      (git_branch_list('origin/release-*') || '').split
    end

    def next_release
      release_branch = fetch_release_branches
        .select { |name| name =~ %r{\Aorigin/release-\d+-\d+\Z} }
        .map { |name| name.delete_prefix('origin/') }
        .max_by { |name| sort_by_version(name) }

      unless release_branch
        puts "Release branch is missing!"
        exit 1
      end

      puts "Detected release: #{release_branch}"
      release_branch
    end

    def sort_by_version(release)
      version = release.match(/(\d+)-(\d+)/)
      [version[1].to_i, version[2].to_i]
    end
  end
end
