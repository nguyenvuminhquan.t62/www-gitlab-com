---
layout: handbook-page-toc
Title: Handbook Migration Plan
Description: Handbook Content is on the move.  This guide shares whats moving and when and why
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The migration plan

### Timetable

| Order | Section                  | Total Pages | Markdown | ERB  |      Start |     Finish |
| :---: | :----------------------- | :---------: | :------: | :--: | ---------: |-----------:|
|   1   | Job Families             |     408     |   408    |  0   | 2023-03-28 | 2023-03-31 |
|   2   | Company Handbook Content |     52      |    49    |  3   | 2023-04-18 | 2023-04-21 |
|   3   | Engineering              |    1144     |   574    | 547  | 2023-05-02 | 2023-08-31 |
|   4   | Legal                    |     67      |    67    |  0   | 2023-05-09 | 2023-05-12 |
|   5   | Infrastructure Standards |     21      |    21    |  0   | 2023-05-16 | 2023-05-19 |
|   6   | IT Self Service          |     12      |    12    |  0   | 2023-05-22 | 2023-05-26 |
|   7   | Security                 |     153     |   148    |  3   | 2023=06-06 | 2023-06-09 |
|   8   | Finance                  |     83      |    67    |  16  | 2023-06-27 | 2023-06-30 |
|   9   | People Group             |     211     |   192    |  18  | 2023-07-11 | 2023-07-14 |
|  10   | Marketing                |     415     |   423    |  44  | 2023-07-25 | 2023-07-28 |
|  11   | Sales                    |     471     |   423    |  44  | 2023-08-08 | 2023-08-11 |
|  12   | Product                  |     414     |   351    |  64  | 2023-09-05 | 2023-09-08 |
|  13   | TeamOps                  |      7      |    0     |  7   |        TBC |        TBC |

### Why this order

The order is based largely on the table of contents for the handbook but its been weighted slightly so that those sections with lots of Ruby Template files (`erb`) are pushed to the bottom of the list.  The idea is that content which is easier to move is the content which moves first.  This will give more time for those teams who are using ruby templating to move their content to markdown.  Where content can't be easily moved to Markdown this will give the Handbook team time to work with teams to find and develop solutions to meet their needs.

We also hope to run in parallel the migration of the Engineering content from the existing handbook to the new handbook which we hope will reduce the time it takes to migrate all of the content from `www-gitlab-com`.  If we are unable to do this we'll still migrate engineering after Company Handbook Content.

### Dates and order subject to change

This is the order we have identified to migrate content but it is subject to change based on factors such as opperational need and how smooth the migration goes for each section.  We won't hesitate to bring forward a migration date if the current peice of content has moved smoothly and quickly.  We'll also communicate changes in [#whats-happening-at-gitlab](https://gitlab.slack.com/archives/C0259241C) and [#handbook](https://gitlab.slack.com/archives/C81PT2ALD) on Slack.

## What is moving next?

**What are we moving: ** Company Handbook Content

**Target Date:** 18th - 21st April 2023

### Why this content

This is content is at the start of the Handbooks Table of Contents.  Its a coillection of smaller sections of the handbook that are widely referenced by other sections of the handbook.  The team responsible for the handbook and its migration sit within the Chief of Staff Team (CoST) so this represented a good opportunity to dogfood the migration.

### What content is moving

The following content has been ear marked as the next content to move.  This will be done as a series of small MRs over the course of a week or so before the end of April.

- [ ] https://about.gitlab.com/handbook/values - /sites/handbook/source/handbook/values
- [ ] https://about.gitlab.com/handbook/only-healthy-constraints - /sites/handbook/source/handbook/only-healthy-constraints
- [ ] https://about.gitlab.com/handbook/documentation/ - /site/handbook/source/handbook/documentation
- [ ] https://about.gitlab.com/handbook/being-a-public-company/ - /sites/handbook/source/handbook/being-a-public-company
- [ ] https://about.gitlab.com/handbook/communications - /sites/handbook/source/handbook/communications
- [ ] https://about.gitlab.com/handbook/about - /sites/handbook/source/handbook/about
- [ ] https://about.gitlab.com/handbook/ceo/ - /sites/handbook/source/handbook/ceo (this just the CEO page and CoST pages)
- [ ] https://about.gitlab.com/faq-gitlab-licensing-technology-to-independent-chinese-company - /sites/handbook/source/handbook/faq-gitlab-licensing-technology-to-independent-chinese-company 
- [ ] https://about.gitlab.com/key-review - /sites/handbook/source/handbook/key-review
- [ ] https://about.gitlab.com/handbook/group-conversations/ - /sites/handbook/source/handbook/group-conversations/
- [ ] https://about.gitlab.com/handbook/e-group-weekly/ - /sites/handbook/source/handbook/e-group-weekly
- [ ] https://about.gitlab.com/handbook/environmental-sustainability/ - /sites/handbook/source/handbook/environmental-sustainability
- [ ] https://about.gitlab.com/handbook/content-websites-responsibility/ - /sites/handbook/source/handbook/content-websites-responsibility (I think we should rename this and add more content)
- [ ] https://about.gitlab.com/handbook/inspired-by-gitlab/ - /sites/handbook/source/handbook/inspired-by-gitlab/
- [ ] https://about.gitlab.com/handbook/about/ - /stes/handbook/source/handbook/about
- [ ] https://about.gitlab.com/handbook/handbook-usage/ - /sites/handbook/source/handbook/handbook-usage
- [ ] https://about.gitlab.com/handbook/CHANGELOG.html
- [ ] https://about.gitlab.com/handbook/practical-handbook-edits/ - /sites/handbook/source/handbook/practical-handbook-edits
- [ ] https://about.gitlab.com/handbook/using-gitlab-at-gitlab - /sites/handbook/source/handbook/using-gitlab-at-gitlab
- [ ] https://about.gitlab.com/handbook/style-guide - /sites/handbook/source/handbook/style-guide

### How will this be done

We hope to do this as a series of small MRs over the course of a week.  Each of these sections is typically only a page or two.  So we shall one small section at a time over the course of 4 days.  Each move is a pair of MRs.  One brings the content over to the new handbook while the other removes the content from `www-gitlab-com` and sets up a redirect to the new content.

If you have any questions about this please feel free to ask in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD) on Slack or reach out directly to @jamiemaynard.

## The break down of whats moving

#### Job Families 

*Expected date:* End of March 2023

*Content to move:*

- [x] Job Families

#### Company Handbook Content

**Expected date:** End of April 2023

**Content to move:**

- [ ] Values
- [ ] Being a public company
- [ ] Communication
- [ ] Handbook
- [ ] CEO/CoST Team
- [ ] Key Reviews
- [ ] Group Conversations
- [ ] E-Group Weekly
- [ ] Sustainability 

- [ ] About the handbook
- [ ] Content Websites Responsibility
- [ ] Style-Guide
- [ ] Inspired By Gitlab

- [ ] Executive Business Administrators

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#company-handbook-content)

#### Legal

*Expected date:* End of May 2023

*Content to move:*

- [ ] DMCA Policy
- [ ] GDPR Policy
- [ ] Legal

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#legal)

#### Infrastructure Standards

*Expected date:* End of May 2023

*Content to move:*

- [ ] Infrastructure Standards

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#infrastructure-standards)

#### IT Self Service

*Expected date:* End of May 2023

*Content to move:*

- [ ] IT Self Service

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#it-self-service)

#### Security

*Expected date:* End of June 2023

*Content to move:*

- [ ] Organizational Change Management
- [ ] Security

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#security)

#### Finance

*Expected date:* End of June 2023

*Content to move:*

- [ ] Board Meetings
- [ ] Finance
- [ ] Internal Audit
- [ ] Spending Company Money
- [ ] Stock Options
- [ ] Tax

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#finance)

#### People Group

*Expected date:* End of July 2023

*Content to move:*

- [ ] Anti-Harassment
- [ ] Entity
- [ ] Hiring
- [ ] Incentives
- [ ] Labor and Employment
- [ ] Leadership
- [ ] Paid Time Off
- [ ] People Group
- [ ] Tools and Tips
- [ ] Total Rewards
- [ ] Travel

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#people-group)

#### Marketing

*Expected date:* End of July 2023

*Content to move:*

- [ ] Marketing
- [ ] Use-Cases

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#marketing)

#### Sales

*Expected date:* End of August 2023

*Content to move:*

- [ ] Alliances
- [ ] Customer Success
- [ ] Resellers
- [ ] Sales

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#sales)

#### Product

*Expected date:* End of August 2023

*Content to move:*

- [ ] Acquisitions
- [ ] Business Technology
- [ ] Product
- [ ] Product Development

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#product)

#### Engineering

*Expected date:* End of August 2023

*Content to move:*

- [ ] Developer Onboarding
- [ ] Engineering
- [ ] Support

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#engineering)

#### TeamOps

*Expected date:* End of August 2023

*Content to move:*

- [ ] TeamOps

A full breakdown of this sections content files can be viewed on our [Migration details page](/handbook/content-websites-responsibility/migration-plan/migration-details/#teamops)

## Refactor and move data

**Target Date:** End of Q2 2023

### Why move data

The contents of the `www-gitlab-com` data directory is a Source of Truth in its own right.  It is shared by a number of functions of the `www-gitlab-com` repository as well as the `internal-handbook`, `digital-experience` and the new `handbook` .  This represents a significant dependancy for all these repositories and the content they drive.

Like the refactor of `www-gitlab-com` refactoring data will allow this dependancy to have a single DRI and provide a single place where all repositories which depend on this data to call on.  Data will be benefit from faster pipelines and changes to data can quickly propegated to thos repositories that depend on them.

### Status

This is curently in the planning stages and we'll share more as soon as we can.
