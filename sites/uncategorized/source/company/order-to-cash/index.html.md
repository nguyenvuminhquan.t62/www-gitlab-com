---
layout: markdown_page
title: "Order to Cash"
description: "GitLab's Order to Cash systems and processes"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

This is cross-functional page which is meant to be the source of truth for our Order to Cash systems.

## Snowflake Data Warehouse and dbt (data build tool)  

We extract data from the Order to Cash systems towards Snowflake and use dbt to transform the data into data models for reporting and analysis.

### Data Architecture Plan

The Fulfillment Team is re-architecting our Order 2 Cash Systems, in particular CustomersDot, in a way that promotes more reliability, sustainability, and flexibility. The plan is described in this [data_architecture.md](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/architecture/data_architecture.md) file. A significant result of the new architecture will be to have the same customer definition in CustomerDot, Zuora, and Salesforce. This definition is based on the Billing Account in Zuora and the key for the customer is the billing_account_id. It is necessary to re-architect the data model in Snowflake and dbt in order to align the customer definition to CustomersDot data tables. The data models built that model the Zuora and Salesforce systems have the correct customer definition and no rearchitecting needs to happen for those models at this time.

**Important Callouts for the Customer Definition in the Unified O2C data model:**

1. The unified customer key is the Billing_Account_Id which joins the systems together. The Billing_Account_Id appears on O2C systems as follows: 
    1. `Zuora.Account.Account_Id`
    1. `CustomersDot.Billing_Accounts.Billing_Account_Id`
    1. `Salesforce.Account.Billing_Account_Id`
    1. `DRAFT: GitLab_Dotcom.Organization.Organization_Id` The Organization Entity Object has not been created yet in GitLab_Dotcom and is still in the validation process.
2. The source id data (where the data is originated and is considered the SSOT) for billing_account_id lives in Zuora.
3. The customer definition is only unified for billing accounts that are in Zuora and Salesforce which are generally limited to paying and formerly paying customers and edu/oss customers. 
4. The target state for trials is to put them into Zuora. At that time, we would be able to have a unified definition for customers across CustomersDot, Salesforce, and Zuora for their trial to paid activity.
5. The target state for how to unify the customer definition for free customers across the O2C systems needs to be determined. The quantity of free users presents challenges for putting them into Salesforce and Zuora. This area needs further exploration.
6. The Account object in Salesforce can have customers that will have a billing account or it can have prospect accounts that do not have a billing account.

Below is the Entity Relationship Diagram for the Re-architected data model in Snowflake. The Target State tab shows how the business entities we extract from the CustomersDot, Zuora, Salesforce, and GitLab.com source systems connect with each other.

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/c8f1520c-e59b-4551-a9db-bfce88bb84dc" id="0GkOGAjoD_O."></iframe></div>

### ERDs (Entity Relationship Diagrams) for Business Process Modeling

These ERDs illustrate how we model data from the Order to Cash Systems in the Snowflake Enterprise Dimensional Model.

<details markdown=1>

<summary><b>Sales Funnel Dimensional Model ERD (Built using Salesforce data)</b></summary>

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/b09f9e0a-e695-4cba-882d-981a93216293" id="7Da6Neo1dhab"></iframe></div>

</details>

<details markdown=1>

<summary><b>Service Ping Dimensional Model ERD (Built with CustomerDot, Version App, Zuora, and Salesforce Data)</b></summary>

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/3a42e56a-028e-45d7-b2ca-5ef489bafd32" id="Z1Mr2IgZz268"></iframe></div>

</details>

<details markdown=1>

<summary><b>Common Subscription Dimensional Model ERD (Built using Salesforce and Zuora Data)</b></summary>

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/7becf6b4-98d8-4e49-8467-764a3296622d" id="T3MrtOiP96Ov"></iframe></div>

</details>

<details markdown=1>

<summary><b>ARR (Annual Recurring Revenue) Dimensional Model ERD (Built using Salesforce and Zuora Data)</b></summary>

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/998dbbae-f04e-4310-9d85-0c360a40a018" id="f5MrUO8fEC1Y"></iframe></div>

</details>

### dbt Data Lineage Diagrams and Data Dictionaries

[Order to Cash Data Lineage Diagrams](/handbook/business-technology/data-team/data-catalog/#dbt-data-lineage-diagrams) illustrate how the data from critical Order to Cash source tables flow through the Snowflake data models.

[Order to Cash Data Dictionaries](/handbook/business-technology/data-team/data-catalog/#dbt-data-dictionaries) provide definitions for the Order to Cash fields used in the Snowflake Enterprise Dimensional Data Model.

### Business Insights and Analysis

Our Data Catalog provides access to Analytics Hubs, Data Guides, ERDs, and Analytics projects relating to the Order to Cash business processes. 

- [Lead to Cash Data Catalog](/handbook/business-technology/data-team/data-catalog/#lead-to-cash-catalog)
- [Product Release to Adoption Data Catalog](/handbook/business-technology/data-team/data-catalog/#product-release-to-adoption-catalog)
