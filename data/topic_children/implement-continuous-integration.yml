description: Implementing continuous integration is a challenge. Find out the
  three essential tools you need and how to do continuous integration the right
  way.
canonical_path: /topics/ci-cd/implement-continuous-integration/
parent_topic: ci-cd
file_name: implement-continuous-integration
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: Why you should implement continuous integration
header_body: "[Continuous integration (CI)](/topics/ci-cd/) is the practice of
  merging code into a shared repository, usually several times a day. How
  continuous integrations works is that within the repository or production
  environment, automated building and testing is done to make sure there are no
  integration issues or problems with the code being merged. Continuous
  integration pipelines conduct automated tests. New code either passes these
  tests and proceeds to the next stage, or fails. This ensures that only
  CI-validated code ever makes it into production. "
cover_image: https://about.gitlab.com/images/blogimages/cicd_pipeline_infograph.png
body: >-
  ## How to set up continuous integration


  Continuous integration done well requires a mindset shift and a commitment to [DevOps best practices](https://about.gitlab.com/topics/devops/). In addition to organizational buy-in, there can be significant investments in infrastructure and tooling to consider. Technical leadership with DevOps knowledge and experience working in a cloud native environment will also be crucial for success.


  ## Essential continuous integration tools


  Teams can invest in any combination of tools or cloud services, but teams implementing continuous integration (CI) for the first time should start with three essential tools:


  * A source code management (SCM) system such as [Git](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/).

  * A shared source code repository that serves as a single source of truth for all code.

  * A continuous integration server that executes scripts for code changes that is integrated with your centralized source code repository (such as [GitLab CI](https://about.gitlab.com/features/continuous-integration/)).


  ## How to do continuous integration the right way

  Continuous integration is more than just tools. While implementing CI tools is part of the process, there is a cultural shift that needs to happen as well. Continuous integration is one part of the larger [DevOps mindset](/topics/devops/). To get the maximum benefits of continuous integration, keep in mind the tools and cultural needs:


  ### Commit code frequently


  Continuous integration thrives with small, frequent changes to code. Code tested in small batches makes it easier for developers to identify bugs and errors and ensures better code quality.


  ### Avoid complexity in CI pipelines


  It’s easy to introduce unnecessary complexity into development environments. Keep things as simple as possible and look for [boring solutions](https://about.gitlab.com/blog/2020/08/18/boring-solutions-faster-iteration/).


  ### Find the right continuous integration for your needs[](https://about.gitlab.com/topics/ci-cd/implement-continuous-integration/#find-the-right-continuous-integration-for-your-needs)


  Not all CI is created equal, so it’s important to [find the right CI](https://about.gitlab.com/topics/ci-cd/choose-continuous-integration-tool/) for your needs. Is it compatible with your cloud provider? Is it within budget? How does it compare to other similar tools? Does it have room for growth? Ask the right questions and you’ll find a CI solution that can help you in the long term.
resources_title: Learn more about continuous integration
resources:
  - url: https://about.gitlab.com/webcast/cicd-in-your-organization/
    type: Webcast
    title: Making the case for CI/CD in your organization
  - title: GitLab vs. Jenkins comparison
    url: https://about.gitlab.com/devops-tools/jenkins-vs-gitlab/
    type: Case studies
suggested_content:
  - url: /blog/2020/09/24/devops-stakeholder-buyin/
  - url: /blog/2019/08/28/building-build-images/
  - url: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
